--- 1
select model, speed, hd 
from pc 
where price < 500


--- 2
Select distinct maker 
from product 
where type = 'Printer'


--- 3
Select model, ram, screen 
from laptop 
where price > 1000


--- 4
Select * 
from printer 
where color = 'y'


--- 5
Select model, speed, hd 
from pc
where price < 600 
and cd in ('12x', '24x')


--- 6
Select distinct maker, speed
from laptop inner join product
on laptop.model = product.model
where hd >= 10


--- 7
select model, price from laptop
where model in 
(
  select model 
  from product 
  where maker = 'B'
)

union

select model, price from pc
where model in 
(
  select model 
  from product 
  where maker = 'B'
)

union

select model, price from printer
where model in 
(
  select model 
  from product 
  where maker = 'B'
)


--- 8
Select maker 
from product
where type = 'pc' except 
(
  select maker 
  from product
  where type = 'laptop'
)


--- 9
Select distinct maker 
from product
where model in 
(
  select model 
  from pc 
  where 
  speed >= 450
)


--- 10
Select model, price 
from printer
where price = 
(
  select max(price) 
  from printer
)


--- 11
Select avg(speed) 
from pc


--- 12
Select avg(speed) 
from laptop 
where price > 1000


--- 13
Select avg(speed) 
from pc
where model in 
(
  select model 
  from product 
  where maker = 'A' 
  and type = 'pc'
)


--- 14
Select maker, min(type) 
from product
group by maker
having count(distinct type) = 1 
and count(model) > 1


--- 15
Select hd from pc
group by hd
having count(code) > 1


--- 16
Select distinct a.model, b.model, a.speed, a.ram
from pc as a, pc as b
where a.speed = b.speed 
and a.ram = b.ram 
and a.model > b.model
order by a.model


--- 17
Select distinct type, laptop.model, speed 
from laptop, product
where speed < 
(
  select min(speed) 
  from pc
) 
and laptop.model = product.model


--- 18
Select distinct maker, price
from printer join product 
on product.model = printer.model
where price = 
(
  select min(price) 
  from printer 
  where color = 'y'
) 
and color = 'y'


--- 19
Select maker, avg(screen) 
from laptop join product 
on laptop.model = product.model
group by maker


--- 20
Select maker, count(model)
from product
where type = 'pc'
group by maker
having count(distinct model) >= 3


--- 21
Select maker, max(price)
from product join pc
on product.model = pc.model
group by maker


--- 22
Select speed, avg(price)
from pc 
where speed > 600
group by speed


--- 23
select maker 
from product, pc
where product.model = pc.model 
and pc.speed >= 750

intersect

select maker 
from product, laptop
where product.model = laptop.model 
and laptop.speed >= 750


--- 24
with cte as
(
  select model, price 
  from pc 
  where price = 
  (
    select max(price) 
    from pc
  )

  union

  select model, price 
  from laptop 
  where price = 
  (
    select max(price) 
    from laptop
  )

  union

  select model, price 
  from printer 
  where price = 
  (
    select max(price) 
    from printer
  )
)

select model 
from cte
where price >= all
(
  select price 
  from cte
)


--- 25
with lowest_ram_cte as 
(
  select * 
  from pc 
  where ram <= all
  (
    select ram 
    from pc
  )
)

Select distinct maker 
from product join pc
on product.model = pc.model
where maker in 
(
  select maker 
  from product 
  where type = 'printer'

  intersect

  select maker 
  from product 
  where type = 'pc' 
)
and code in 
(
  select code 
  from lowest_ram_cte
)
and speed >= all
(
  select speed 
  from lowest_ram_cte
)


--- 26
Select avg(price) as AVG_price 
from
(
  select price 
  from pc, product
  where pc.model = product.model 
  and product.maker = 'A'

  union all

  select price 
  from laptop, product
  where laptop.model = product.model 
  and product.maker = 'A'
) as myalias


--- 27
select maker, avg(hd) 
from pc join product 
on pc.model = product.model
where maker in 
(
  select maker 
  from product 
  where type='printer'
)
group by maker


--- 28
select cast(cast(sum(coalesce(b_vol, 0)) as numeric(8,2)) 
/ 
count(distinct q_id) as numeric(8,2))  
as avg_paint
from utb right join utq 
on utb.b_q_id = utq.q_id


--- 29
select i1.point, i1.date, i1.inc, o1.out
from income_o i1 left join outcome_o o1
on i1.point = o1.point 
and i1.date = o1.date 

union

select o2.point, o2.date, i2.inc, o2.out
from income_o i2 right join outcome_o o2
on i2.point = o2.point 
and i2.date = o2.date


--- 30
select i.point, i.date, o.out, i.inc
from
(
  select point, date, sum(inc) as inc
  from Income
  group by point, date
) i
left join
(
  select point, date, sum(out) as out
  from Outcome
  group by point, date
) o
on i.point = o.point
and i.date = o.date

union

select o.point, o.date, o.out, i.inc
from
(
  select point, date, sum(out) as out
  from Outcome
  group by point, date
) o
left join
(
  select point, date, sum(inc) as inc
  from Income
  group by point, date
) i
on o.point = i.point
and o.date = i.date


--- 31
Select class, country 
from classes
where bore >= 16


--- 32
select country, cast(avg(power(bore,3)/2) as decimal(10,2)) as weight 
from
(
  select country, bore, ship
  from classes, outcomes
  where classes.class= outcomes.ship

  union

  select country, bore, name
  from classes, ships
  where classes.class = ships.class
) all_ships
group by country


--- 33
Select ship
from outcomes
where battle = 'North Atlantic' 
and result = 'sunk'


--- 34
Select name
from ships, classes
where ships.class = classes.class
and type='bb'
and ships.launched >= 1922
and classes.displacement >= 35000


--- 35
Select model, type
from product
where model like replicate('[A-Z]', len(model))
or model like replicate('[0-9]', len(Model))


--- 36
Select name from ships
where name = class

union

select ship 
from outcomes, classes
where outcomes.ship = classes.class


--- 37
select class from
(
  Select classes.class, name as ship_name
  from classes join ships
  on classes.class = ships.class
  group by classes.class, name

  union

  select classes.class, ship as ship_name
  from classes join outcomes
  on classes.class = outcomes.ship
  group by class, ship
) all_ships
group by class
having count(ship_name) = 1


--- 38
Select country 
from classes
where type = 'bb'

intersect

select country 
from classes
where type = 'bc'


--- 39
select distinct o1_b1.ship 
from
(
  select ship, result, date
  from outcomes join battles
  on battle = name
) o1_b1
join
(
  select ship, result, date
  from outcomes join battles
  on battle = name
) o2_b2
on o1_b1.ship = o2_b2.ship
where o1_b1.result = 'damaged' 
and o1_b1.date < o2_b2.date


--- 40
Select classes.class, name, country
from classes, ships
where classes.class = ships.class
and numGuns >= 10


--- 41
Select 'model', model 
from pc
where code = 
(
  select max(code) 
  from pc
)

union

select 'speed', cast(speed as varchar(50)) 
from pc
where code = 
(
  select max(code) 
  from pc
)

union

select 'ram', cast(ram as varchar(50)) 
from pc
where code = 
(
  select max(code) 
  from pc
)

union

select 'hd', cast(hd as varchar(50)) 
from pc
where code = 
(
  select max(code) 
  from pc
)

union

select 'cd', cast(cd as varchar(50)) 
from pc
where code = 
(
  select max(code) 
  from pc
)

union

select 'price', cast(price as varchar(50)) 
from pc
where code = 
(
  select max(code) 
  from pc
)


--- 42
Select distinct ship, battle
from outcomes
where result = 'sunk'


--- 43
select name 
from battles
where year(date) not in
(
  select launched 
  from ships
  where launched is not null
)


--- 44
Select name 
from ships
where name like 'R%'

union

select ship 
from outcomes
where ship like 'R%'


--- 45
select name
from Ships
where name like '% % %'

union

select ship
from Outcomes
where ship like '% % %'


--- 46
Select distinct ship, displacement, numGuns
from classes left join ships
on classes.class = ships.class
right join outcomes
on outcomes.ship = ships.name 
or outcomes.ship = classes.class
where battle = 'Guadalcanal'


--- 47
with cte1 as
(
  select maker, count(*) as maker_models
  from product
  group by maker
)
,cte2 as
(
  select product.maker, maker_models, product.model
  from product, cte1
  where product.maker = cte1.maker
)

select count(*), p1.maker, p1.model
from cte2 p1, cte2 p2
where
(
  p1.maker_models < p2.maker_models
)
or
(
  p1.maker_models = p2.maker_models 
  and p1.maker > p2.maker
)
or
(
  p1.maker_models = p2.maker_models 
  and p1.maker = p2.maker 
  and p1.model >= p2.model
)
group by p1.maker, p1.model


--- 48
select class
from classes, outcomes
where class = ship
and result = 'sunk'

union

select class
from ships, outcomes
where name = ship
and result = 'sunk'


--- 49
Select name
from ships, classes
where ships.class = classes.class
and bore = 16

union

select ship
from outcomes, classes
where ship = class
and bore = 16


--- 50
Select distinct battle
from outcomes, ships
where ship = name
and class = 'kongo'


--- 51
with ship_match as
(
  select distinct classes.class, numGuns, displacement, name
  from classes, ships
  where classes.class = ships.class

  union

  select distinct class, numGuns, displacement, ship as name
  from classes, outcomes
  where class = ship
)

select name 
from ship_match sm1
where numGuns >= all
(
  select numGuns 
  from ship_match sm2
  where sm1.displacement = sm2.displacement
)


--- 52
select distinct name from
(
  Select name, country, type, numGuns, bore, displacement
  from ships, classes
  where ships.class = classes.class

  union

  select ship as name, country, type, numGuns, bore, displacement
  from outcomes, classes
  where outcomes.ship = classes.class
) result
where 
case when country is null then 'Japan'
     else country
end = 'Japan'
and 
case when type is null then 'bb'
     else type
end = 'bb'
and 
case when numGuns is null then 9
     else numGuns
end >= 9
and 
case when bore is null then 18
     else bore
end < 19
and 
case when displacement is null then 65000
     else displacement
end <= 65000


--- 53
select cast(avg(
  case when numGuns is null then 0.0
       else numGuns
  end
) as numeric(10,2))
from classes
where type = 'bb'


--- 54
with cte1 as
(
  select numGuns, name
  from classes, ships
  where classes.class = ships.class
  and type = 'bb'

  union

  select numGuns, ship
  from classes, outcomes
  where class = ship
  and type = 'bb'
)

select cast(avg(numGuns+0.0) as numeric(10,2))
from cte1


--- 55
with cte1 as
(
  select classes.class, min(launched) as year
  from classes, ships
  where classes.class = ships.class
  group by classes.class
)

select class, year 
from cte1

union

select classes.class, null as year
from classes 
where classes.class not in
(
  select class 
  from cte1
)


--- 56
with cte1 as
(
  select classes.class
  from  classes, ships, outcomes
  where classes.class = ships.class
  and ship = name
  and result = 'sunk'

  union all

  select class
  from classes, outcomes
  where class = ship
  and result = 'sunk'
  and ship not in (select name from ships)
)

select class, (select count(1) from cte1 where classes.class = cte1.class)
from classes
